#!/usr/bin/python3

INP = "input.txt"
OUT = "output.txt"
X16 = 2**16
import sys

def Value(x):
    if x is None:
        return None
    if x > X16:
        return hex(x)
    return x

class Task():

    def __init__(self, temp):
        self.rars = None
        if (temp[0] != "Zp") and (temp[0] != "GF2M"):
            raise ValueError("Неправильное поле или его нет")
        if temp[1][0] != "P":
            raise ValueError("Нет модуля")
        if temp[2][0] != "A":
            raise ValueError("Нет коэфициента A")
        if temp[3][0] != "B":
            raise ValueError("Нет коэфициента B")
        if (temp[4][0] != "+") and (temp[4][0] != "*"):
            raise ValueError("Нет операции")
        if temp[5][0] != "N":
            raise ValueError("Нет числа пар точек N")
        self.field = temp[0]
        if (temp[0] == "Zp"):
            self.sprime = self.getInt(temp[1], 'P')
            self.prime = self.getInt(temp[1], 'P')
            self.A = self.getInt(temp[2], 'A')
            self.B = self.getInt(temp[3], 'B')
            self.Oper = temp[4][0]
            self.N = self.getInt(temp[5], 'N')
            self.ars = []
            if self.Oper == "+":
                for i in range(self.N):
                    self.ars.append((self.getPoint(temp[i*2+6], i, 1), self.getPoint(temp[i*2+7], i, 2)))
            else:
                for i in range(self.N):
                    K=self.getInt(temp[i*2+6], 'K')
                    self.ars.append((K, self.getPoint(temp[i*2+7], i, 1)))
        if (temp[0] == "GF2M"):
            self.sprime = self.getBinVec(temp[1])
            self.prime = self.getBinInt(temp[1])
            self.A = self.getInt(temp[2], 'A')
            self.B = self.getInt(temp[3], 'B')
            self.Oper = temp[4][0]
            self.N = self.getInt(temp[5], 'N')
            self.ars = []
            if self.Oper == "+":
                for i in range(self.N):
                    self.ars.append((self.getPoint(temp[i * 2 + 6], i, 1), self.getPoint(temp[i * 2 + 7], i, 2)))
            else:
                for i in range(self.N):
                    K = self.getInt(temp[i * 2 + 6], 'K')
                    self.ars.append((K, self.getPoint(temp[i * 2 + 7], i, 1)))


    def getBinVec(self, l):
        line = l[1:]
        line = line.replace('=', '')
        line = line.replace(' ', '')
        return line

    def getBinInt(self, l):
        line = self.getBinVec(l)
        line = line.replace('(', '')
        line = line.replace(')', '')
        line = line.split(',')
        p = 0
        for i in line:
            try:
                k = int(i)
            except:
                try:
                    k = int(i, 16)
                except:
                    raise ValueError("Плохой коэфициент P {}".format(i))
            p = p^(1<<k)
        return p

    def __str__(self):
        S = "{}\nP = {}\nA = {}\nB = {}\n{}\nN = {}\n".format(
            self.field,
            self.sprime,
            Value(self.A),
            Value(self.B),
            self.Oper,
            self.N
        )
        if self.Oper == "+":
            if self.rars is None:
                for i in range(self.N):
                    S+="({},{}){}({},{})=\n=\n\n".format(Value(self.ars[i][0][0]), Value(self.ars[i][0][1]), self.Oper, Value(self.ars[i][1][0]), Value(self.ars[i][1][1]))
            else:
                for i in range(self.N):
                    S+="({},{}){}({},{})=\n={}\n\n".format(Value(self.ars[i][0][0]), Value(self.ars[i][0][1]), self.Oper, Value(self.ars[i][1][0]), Value(self.ars[i][1][1]), self.rars[i])
        else:
            if self.rars is None:
                for i in range(self.N):
                    S+="({})({},{})=\n=\n\n".format(Value(self.ars[i][0]), self.Oper, Value(self.ars[i][1][0]), Value(self.ars[i][1][1]))
            else:
                for i in range(self.N):
                    S+="({}){}({},{})=\n={}\n\n".format(Value(self.ars[i][0]), self.Oper, Value(self.ars[i][1][0]), Value(self.ars[i][1][1]), self.rars[i])
        return S

    def getInt(self, l, C):
        line = l[1:]
        i = 0
        while((line[i] == " ") or (line[i] == "=")):
            i += 1
            if i == (len(line)):
                raise ValueError("Нет коэфициента {}".format(C))
        try:
            return int(line[i:])
        except:
            try:
                return int(line[i:], 16)
            except:
                raise ValueError("Плохой коэфициент {}".format(C))

    def getPoint(self, l, i, j):
        line = l[1:]
        line = line.replace(' ', '')
        line = line.replace('(', '')
        line = line.replace(')', '')
        line = line.split(',')
        if (line[0] == 'None') and (line[1] == "None"):
            x=None
            y=None
            return x, y
        try:
            x = int(line[0])
        except:
            try:
                x = int(line[0], 16)
            except:
                raise ValueError("Плохой коэфициент X({},{})".format(i, j))
        try:
            y = int(line[1])
        except:
            try:
                y = int(line[1], 16)
            except:
                raise ValueError("Плохой коэфициент Y({},{})".format(i, j))
        return x, y

    def generate(self):
        if self.field == "Zp":
            curve = CurveZp(self.prime, self.A, self.B)
            self.rars = []
            if self.Oper == "+":
                for i in range(self.N):
                    Q = PointZp(curve, self.ars[i][0][0], self.ars[i][0][1])
                    P = PointZp(curve, self.ars[i][1][0], self.ars[i][1][1])
                    self.rars.append(Q+P)
            if self.Oper == "*":
                for i in range(self.N):
                    K = self.ars[i][0]
                    P = PointZp(curve, self.ars[i][1][0], self.ars[i][1][1])
                    self.rars.append(P.mulk(K))
        else:
            curve = CurveGF2M(self.prime, self.A, self.B)
            self.rars = []
            if self.Oper == "+":
                for i in range(self.N):
                    Q = PointGF2M(curve, self.ars[i][0][0], self.ars[i][0][1])
                    P = PointGF2M(curve, self.ars[i][1][0], self.ars[i][1][1])
                    self.rars.append(Q+P)
            if self.Oper == "*":
                for i in range(self.N):
                    K = self.ars[i][0]
                    P = PointGF2M(curve, self.ars[i][1][0], self.ars[i][1][1])
                    self.rars.append(P.mulk(K))


def readin():
    inp = open(INP, "r")
    k=0
    temp = []
    for line in inp:
        if line[0] == "#":
            continue
        elif line == "\n":
            continue
        else:
            temp.append(line[:len(line)-1])
    return Task(temp)


def main():
    global INP
    global OUT
    if len(sys.argv) == 2:
        INP = sys.argv[1]
    elif len(sys.argv) == 3:
        INP = sys.argv[1]
        OUT = sys.argv[2]
    elif len(sys.argv) != 1:
        print("USE: curves [input.txt] [output.txt]")
        return
    T = readin()
    T.generate()
    out = open(OUT, "w")
    out.write(str(T))

# bitlength of a

def GF2Mlength(a):
    k = 0
    while True:
        a >>= 1
        k += 1
        if (a == 0): break
    return k

# a - GF2MValue(polynom), b - GF2MValue(polynom) -> result: (a//b, a%b)

def GF2Mdivmod(a, b):
    na = GF2Mlength(a)
    nd = GF2Mlength(b)
    i = na - nd
    q = 0
    r = a
    while i >= 0:
        rnew = r ^ (b << i)
        if rnew < r:
            q |= (1 << i)
            r = rnew
        i -= 1
    return (q,r)


# E: y^2 + xy = X^3 + Ax^2 + B

class GF2MValue():

    def __init__(self, prime, value):
        temp, self.value=GF2Mdivmod(value, prime)
        self.prime=prime

    def __add__(self, other):
        return GF2MValue(self.prime, self.value^other.value)

    def __sub__(self, other):
        return GF2MValue(self.prime, self.value ^ other.value)

    def __eq__(self, other):
        return (self.prime == other.prime) and (self.value == other.value)

    def __mul__(self, other):
        k = other.value
        l = 0
        m = self.value
        while(k > 0):
            if(k & 1):
                l = l ^ m
            m <<= 1
            k >>= 1
        return GF2MValue(self.prime, l)

    def __invert__(self):
        a = self.prime
        b = self.value
        arow = [a, 1, 0]
        brow = [b, 0, 1]
        while True:
            (q, r) = GF2Mdivmod(arow[0], brow[0])
            if r == 0:
                break
            #rrow = [r, arow[1]-q*brow[1], arow[2]-q*brow[2]]
            rrow = [r, (GF2MValue(self.prime, arow[1]) - GF2MValue(self.prime, q) * GF2MValue(self.prime, brow[1])).value, (GF2MValue(self.prime, arow[2]) - GF2MValue(self.prime, q) * GF2MValue(self.prime, brow[2])).value]
            arow = brow
            brow = rrow
        return GF2MValue(self.prime, tuple(brow)[2])

    def __truediv__(self, other):
        return self*(~other)

    def __int__(self):
        return self.value

    def __str__(self):
        return "{}".format(Value(self.value))

class CurveGF2M():

    def __init__(self, prime, A, B):
        self.a = GF2MValue(prime, A)
        self.b = GF2MValue(prime, B)
        self.prime = prime

class PointGF2M():

    def __init__(self, curve, x=None, y=None):
        self.curve = curve
        if x is None:
            self.x = None
        else:
            self.x = GF2MValue(self.curve.prime, int(x))
        if y is None:
            self.y = None
        else:
            self.y = GF2MValue(self.curve.prime, int(y))
        if not self.onCurve():
            raise ValueError

    def __add__(self, other):
        if (self.x is None) or (self.y is None):
            return other
        elif (other.x is None) or (other.y is None):
            return self
        if (self.x == other.x):
            if (other.y == (self.y + self.x)):
                return PointGF2M(self.curve)
            else:
                k = (self.x*self.x + self.y) / self.x
                l = self.x*self.x
        else:
            k = (other.y + self.y)/(other.x + self.x)
            l = (self.y*other.x + other.y*self.x)/(other.x+self.x)
        x3 = k*k+k+self.curve.a+self.x+other.x
        y3 = (k+GF2MValue(self.curve.prime, 1))*x3+l
        return PointGF2M(self.curve, x3, y3)

    def mulk(self, k):
        if k == 0:
            return PointGF2M(self.curve)
        P = self
        if k < 0:
            P = PointGF2M(self.curve, P.x, P.y+P.x)
            k=abs(k)
        Z = PointZp(self.curve)
        while (k > 0):
            if (k & 1):
                Z = Z + P
            k >>= 1
            P = P+P
        return Z

    def __str__(self):
        return '({}, {})'.format(self.x, self.y)

    def onCurve(self):
        if (self.x is None) or (self.y is None):
            return True
        else:
            return (self.y * self.y) + self.x*self.y == (self.x*self.x*self.x) + self.curve.a*self.x*self.x + self.curve.b

# E: y^2 = x^3 + Ax + B (mod prime)

class ZpValue():

    def __init__(self, prime, value):
        self.value = value % prime
        self.prime = prime


    def __add__(self, other):
        return ZpValue(self.prime, (self.value + other.value))

    def __sub__(self, other):
        return ZpValue(self.prime, (self.value - other.value))

    def __mul__(self, other):
        return ZpValue(self.prime, (self.value * other.value))

    def __eq__(self, other):
        return (self.prime == other.prime) and (self.value == other.value)

    def __neg__(self):
        return ZpValue(self.prime, (-self.value))

    def __int__(self):
        return self.value % self.prime

    def __invert__(self):
        x2, x1, y2, y1 = 1, 0, 0, 1
        q = self.value
        p = self.prime
        while q > 0:
            z, r = divmod(p, q)
            x = x2 - z * x1
            y = y2 - z * y1
            p = q
            q = r
            x2 = x1
            x1 = x
            y2 = y1
            y1 = y
        return ZpValue(self.prime, y2)

    def __truediv__(self, other):
        return self*(~other)

    def __str__(self):
        return "{}".format(Value(self.value))

class CurveZp():

    def __init__(self, prime, A, B):
        self.a = ZpValue(prime, A)
        self.b = ZpValue(prime, B)
        self.prime = prime
        self.check()

    def check(self):
        if ZpValue(self.prime, 4)*self.a*self.a*self.a + ZpValue(self.prime, 27)*self.b*self.b == ZpValue(self.prime, 0):
            raise TypeError ("Дескриминант = 0")

class PointZp():

    def __init__(self, curve, x=None, y=None):
        self.curve = curve
        if x is None:
            self.x = None
        else:
            self.x = ZpValue(self.curve.prime, int(x))
        if y is None:
            self.y = None
        else:
            self.y = ZpValue(self.curve.prime, int(y))
        if not self.onCurve():
            raise ValueError("({}, {})\nТочка не лежит на кривой".format(self.x, self.y))

    def __add__(self, other):
        if (self.x is None) or (self.y is None):
            return other
        elif (other.x is None) or (other.y is None):
            return self
        if (self.x == other.x):
            if (self.y == other.y):
                k = (ZpValue(self.curve.prime, 3)*self.x*self.x+self.curve.a)/(ZpValue(self.curve.prime, 2)*self.y)
            else:
                return PointZp(self.curve)
        else:
            k = (other.y - self.y)/(other.x - self.x)
        x3 = k*k - self.x - other.x
        y3 = k*(self.x-x3)-self.y
        return PointZp(self.curve, x=x3, y=y3)

    def mulk(self, k):
        if k == 0:
            return PointZp(self.curve)
        P = self
        if k < 0:
            P = PointZp(self.curve, P.x, -P.y)
            k=abs(k)
        Z = PointZp(self.curve)
        while (k > 0):
            if (k & 1):
                Z = Z + P
            k >>= 1
            P = P+P
        return Z

    def onCurve(self):
        if (self.x is None) or (self.y is None):
            return True
        else:
            return (self.y * self.y) == (self.x*self.x*self.x) + self.curve.a*self.x + self.curve.b

    def __str__(self):
        return "({}, {})".format(self.x, self.y)


if __name__ == "__main__":
    main()